# Boxinator Frontend

Below the instructions for installing and using the frontend of the application Boxinator is presented.

Link to live application: https://box1nator-app.herokuapp.com/

## Requiered installations

The following programs, libraries, languages is requiered to be downloaded and installed. <br>
**Node.js** - https://nodejs.org/en/download/ <br>
**Npm** (Is installed with Node.js) - https://www.npmjs.com/package/download <br>

## Using the Application

To change connecting URL (to the Boxinator API) go to /utils/utils.js.

### `npm install`

Installs all dependencies needed to run the application.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

Boxinator was created by Andreas, Oskar & Jacob.