import React, { useState, useEffect } from 'react'
import { Table } from '../../components/table/Table'
import { logOut } from '../../services/AuthService'
import userApi from '../../services/userapi'
import styles from './CountrySettingsView.module.css'

import Alerter from '../../components/alerter/Alerter'

/**
 * A view for the Admin's country settings page.
 * @param {*} props
 */

export default function CountrySettingsView(props) {
  const [data, setData] = useState([])
  const [columns, setColumns] = useState([])

  const [multiplier, setmultiplier] = useState('')
  const [changeId, setChangeId] = useState('')

  const [newCountry, setNewCountry] = useState('')
  const [newCountryMult, setNewCountryMult] = useState('')

  const [showAlert, setShowAlert] = useState(false)
  const [alertInfo, setAlertInfo] = useState({
    title: '',
    message: ''
  })

  const setColumnsF = () => {
    setColumns([
      {
        Header: 'Land',
        accessor: 'countryName'
      },
      {
        Header: 'Multiplier',
        accessor: 'multiplier'
      }
    ])
  }
  useEffect(() => {
    setColumnsF()
    userApi('GET', {}, '/settings/countries')
      .then((resp) => {
        setData(resp.data.data)
      })
      .catch(err => {
        if (err.response) {
          if (err.response) {
            if (err.response.status === 401) {
              logOut()
            }
          }
        }
      })
  }, [])

  // TO CHANGE COUNTRY
  const changeCountryClick = () => {
    userApi('PATCH', multiplier, '/settings/countries/' + changeId)
      .then((resp) => {
        setAlertInfo({
          title: 'Landet uppdaterad',
          message: 'Du har uppdaterat landet'
        })
        const obj = [...data]

        for (let i = 0; i < obj.length; i++) {
          if (obj[i].id === changeId) {
            obj[i].multiplier = multiplier
          }
        }
        setData(obj)

      }).catch(err => {
        if (err.response.status === 400) {
          setAlertInfo({
            title: 'Misslyckad',
            message: 'Fel multiplier'
          })
        }

      }).finally(() => {
        setShowAlert(true)
        //Resetting the state-values
        setNewCountry('')
        setNewCountryMult('')
        setmultiplier('')
        setChangeId('')
      })
  }
  const onMultChange = (value) => {
    setmultiplier(value)
  }
  const collectId = (value) => {
    setChangeId(value.id)
  }

  // ADD NEW COUNTRY
  const newCountryClick = () => {
    userApi('POST', { countryName: newCountry, multiplier: newCountryMult }, '/settings/countries/')
      .then((resp) => {
        setAlertInfo({
          title: 'Landet skapat',
          message: 'Landet har skapats!'
        })
        const tempId = resp.data.data.id
        const obj = [...data]

        obj.push({ id: tempId, countryName: newCountry, multiplier: newCountryMult })

        setData(obj)
      }).catch(err => {
        if (err.response.status === 400) {
          if ((err.response.data.message === "Wrong multiplier") || (err.response.data.message === "Wrong country")) {
            setAlertInfo({
              title: 'Misslyckad',
              message: err.response.data.message
            })
          }
          else {
            setAlertInfo({
              title: 'Misslyckad',
              message: 'Ett fel inträffade'
            })
          }
        }
        else {
          setAlertInfo({
            title: 'Misslyckad',
            message: 'Server fel'
          })
        }
      })
      .finally(() => {
        setShowAlert(true)
        //Resetting the state-values
        setNewCountry('')
        setNewCountryMult('')
        setmultiplier('')
        setChangeId('')
      })
  }
  const infoNewCountry = (e) => {
    const eLow = e.toLowerCase();
    const eCap = (eLow.charAt(0).toUpperCase() + eLow.slice(1))
    setNewCountry(eCap)
  }
  const infoNewCountryMult = (e) => {
    setNewCountryMult(e)
  }

  // DELETE land
  const deleteCountryClick = () => {
    userApi('DELETE', {}, '/settings/countries/' + changeId)
      .then((resp) => {
        setAlertInfo({
          title: 'Landet raderat',
          message: 'Du har raderat landet'
        })
        const obj = [...data]
        for (let i = 0; i < obj.length; i++) {
          if (obj[i].id === changeId) {
            obj.splice(i, 1)
          }
        }
        setData(obj)
      })
      .finally(() => {
        setShowAlert(true)
      })
  }


  return (
    <>
      <Alerter show={showAlert} hide={() => setShowAlert(false)} title={alertInfo.title} message={alertInfo.message} />
      <div className='country-view'>

        <div className={styles.innerContainer}>

          <Table
            typeOfTable={2}
            modalOutgoing={2}
            createNewCountry={newCountryClick}
            newCountryFunc={infoNewCountry}
            NewMultFunc={infoNewCountryMult}
            settingsChangeFunc={changeCountryClick}
            countryId={collectId}
            settingsMultFunc={onMultChange}
            deleteCountryFunc={deleteCountryClick}
            columns={columns} data={data}
          />

        </div>
      </div>

    </>
  )
}
