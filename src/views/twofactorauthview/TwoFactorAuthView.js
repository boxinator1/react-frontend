import React from 'react'
import Logo from '../../components/logo/Logo'
import TwoFactorAuthContainer from '../../components/twofactorauthcontainer/TwoFactorAuthContainer'
import styles from './TwoFactorAuthView.module.css'
export default function TwoFactorAuthView () {
  return (
    <div className='views'>
      <div className={styles.innerContainer}>
        <Logo />
        <TwoFactorAuthContainer />
      </div>
    </div>
  )
}
