import React from 'react'
import LostPasswordContainer from '../../components/lostpasswordcontainer/LostPasswordContainer'
export default function LostPasswordView () {
  return (
    <div className='views'>
      <LostPasswordContainer />
    </div>
  )
}
