import React from 'react'
import OrderSentContainer from '../../components/ordersentcontainer/OrderSentContainer'
import styles from './VerifiedOrderView.module.css'
export default function VerifiedOrderView () {
  return (
    <div className='views'>
      <div className={styles.innerContainer}>
        <OrderSentContainer reset={() => window.location = '/'} />
      </div>
    </div>
  )
}
