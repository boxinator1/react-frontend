import React, { useState } from 'react'
import LoginContainer from '../../components/logincontainer/LoginContainer'
import TwoFactorAuthContainer from '../../components/twofactorauthcontainer/TwoFactorAuthContainer'
import Logo from '../../components/logo/Logo'
import styles from './LoginView.module.css'
export default function LoginView ({ verified = false }) {
  const [email, setEmail] = useState('')
  return (
    <div className='views'>
      <div className={styles.innerContainer}>
        <Logo />
        {email === ''
          ? <LoginContainer passEmailFunc={setEmail} verified={verified} />
          : <TwoFactorAuthContainer email={email} />}
      </div>
    </div>
  )
}
