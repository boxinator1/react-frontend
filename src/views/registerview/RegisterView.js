import React from 'react'
import Logo from '../../components/logo/Logo'
import UserInfoContainer from '../../components/userinfocontainer/UserInfoContainer'
import styles from './RegisterView.module.css'
/**
 * A view for the register page.
 * @param {*} props
 */
export default function RegisterView (props) {
  return (
    <div className='views'>
      <div className={styles.innerContainer}>
        <Logo />
        <UserInfoContainer props={props} register />
      </div>
    </div>
  )
}
