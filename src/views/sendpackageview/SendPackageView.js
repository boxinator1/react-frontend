import React from 'react'
import SendPackageContainer from '../../components/sendpackagecontainer/SendPackageContainer'
export default function SendPackageView () {
  return (
    <div className='views'>
      <SendPackageContainer />
    </div>
  )
}
