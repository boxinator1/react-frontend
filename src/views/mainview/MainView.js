import React from 'react'
import Table from '../../components/table/Table'
import styles from './MainView.module.css'
/**
 * A view for the register page.
 * @param {*} props
 */
export default function MainView (props) {
  return (
    <div className='main-view'>
      <div className={styles.innerContainer}>
        <Table />
      </div>
    </div>
  )
}
