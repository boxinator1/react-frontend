import axios from 'axios'
import { apiUrl } from '../utils/utils'

/**
*    Function that handles all user requests to the api with the axios library.
*    @param reqMethod = String POST, PATCH, DELETE etc...
*    @param reqBody = Object of data that will be sent in req body to the api.
*    @param reqPath = String declare the endpoint to the api. account, shipment etc. No forward slash
*/

const userApi = (reqMethod, reqBody, reqPath) => {
  return new Promise((resolve, reject) => {
    const headers = {}

    headers.Accept = 'application/json'
    headers['Content-Type'] = 'application/json'
    headers.Authentication = localStorage.getItem('authKey') || ''

    const config = {
      method: reqMethod,
      data: reqBody,
      url: apiUrl + reqPath,
      headers: { ...headers }
    }
    axios(config)
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err)
      })
  })
}

export default userApi
