function isLoggedIn () {
  return localStorage.getItem('authKey') != null
}

function getKey () {
  return localStorage.getItem('authKey')
}

function logIn (key, type) {
  localStorage.setItem('authKey', key)
  localStorage.setItem('accountType', type)
  window.location.reload()
}

function logOut () {
  localStorage.removeItem('authKey')
  localStorage.removeItem('accountType')
  window.location.reload()
}

function isAdmin () {
  return localStorage.getItem('accountType') === 'ADMINISTRATOR'
}
export { isLoggedIn, getKey, logIn, logOut, isAdmin }
