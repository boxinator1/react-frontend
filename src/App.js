import React from 'react'
import './App.css'

import RegisterView from './views/registerview/RegisterView'
import Navbar from './components/navbar/Navbar'
import LoginView from './views/loginview/LoginView'
import { isLoggedIn, isAdmin } from './services/AuthService'
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom'
import SendPackageView from './views/sendpackageview/SendPackageView'
import MainView from './views/mainview/MainView'

import CountrySettingsView from './views/countrysettingsview/CountrySettingsView'

import VerifiedOrderView from './views/verifiedorderview/VerifiedOrderView'
import EditUserView from './views/edituserview/EditUserView'
import LostPasswordView from './views/lostpasswordview/LostPasswordView'

function App () {
  return (
    <>
      <Router>
        <Navbar />
        <div className='App'>
          <Switch>
            <UnAuthRouteCheck exact path='/' component={SendPackageView} />
            <AdminRouteCheck path='/countrysettings' component={CountrySettingsView} />
            <AuthRouteCheck path='/main' component={MainView} />
            <AuthRouteCheck path='/edituser' component={EditUserView} />
            <UnAuthRouteCheck path='/verifiedlogin' component={() => <LoginView verified />} />
            <UnAuthRouteCheck path='/login' component={LoginView} />
            <UnAuthRouteCheck path='/account' component={RegisterView} />
            <UnAuthRouteCheck path='/verifiedorder' component={VerifiedOrderView} />
            <UnAuthRouteCheck path='/lost' component={LostPasswordView} />
            <Route path='/*' component={SendPackageView} />
          </Switch>
        </div>
      </Router>
    </>
  )
}

export default App

function AuthRouteCheck ({ component: Component, ...rest }) {
  const history = useHistory()
  if (!isLoggedIn()) {
    history.push('/login')
    return null
  } else {
    return (
      <Route
        {...rest}
        render={props => <Component {...props} />}
      />
    )
  }
}

function UnAuthRouteCheck ({ component: Component, ...rest }) {
  const history = useHistory()
  if (isLoggedIn()) {
    history.push('/main')
    return null
  } else {
    return (
      <Route
        {...rest}
        render={props => <Component {...props} />}
      />
    )
  }
}

function AdminRouteCheck ({ component: Component, ...rest }) {
  const history = useHistory()
  if (!isLoggedIn()) {
    history.push('/login')
    return null
  } else if (!isAdmin()) {
    history.push('/main')
    return null
  } else {
    return (
      <Route
        {...rest}
        render={props => <Component {...props} />}
      />
    )
  }
}
