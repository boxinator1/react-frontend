// eslint-disable-next-line
const validateEmail = (email) => /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)

const apiUrl = 'https://box1nator.herokuapp.com/'

module.exports = { validateEmail, apiUrl }
