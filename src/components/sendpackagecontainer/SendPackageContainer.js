import React, { useState } from 'react'
import { validateEmail } from '../../utils/utils'
import Logo from '../logo/Logo'
import InputWrapper from '../inputwrapper/InputWrapper'
import Button from '../button/Button'
import styles from './SendPackageContainer.module.css'
import ConfirmInformationTable from '../confirminformationtable/ConfirmInformationTable'
import OrderSentContainer from '../ordersentcontainer/OrderSentContainer'
import userApi from '../../services/userapi'
import VerificationSentContainer from '../verificationsentcontainer/VerificationSentContainer'
import Loader from '../loader/Loader'
import Alerter from '../alerter/Alerter'

/**
 * This container handels all the different flows when sending a package.
 * In the stateType you can see the first input types for the first three states.
 * Each of the inputs append information to our values state.
 * After accepting the information the user will be send to either {@link ../ordersentcontainer/OrderSentContainer} or {@link ../verificationsentcontainer/VerificationSentContainer}
 * depending on wether or not they are authenticated.
 */
export default function SendPackageContainer ({ hideEmail = null, newOrderFunc = null }) {
  const [state, setState] = useState(0)
  const [values, setValues] = useState({
    boxColor: '#FFFFFF'
  })
  const [loading, setLoading] = useState(false)
  const [showError, setShowError] = useState(false)
  const [error, setError] = useState({
    title: '',
    message: ''
  })
  const reset = () => {
    const v = {}
    v.email = values.email
    setValues(v)
    setState(0)
  }
  // Function for setting a new value in the state state.
  const setNewValue = (type, value) => {
    const v = values
    v[type] = value
    setValues(v)
  }
  const sendOrder = () => {
    setLoading(true)
    userApi('POST', values, 'shipments').then(resp => {
      setState(state + 1)
      setLoading(false)
      if (newOrderFunc !== null && resp.data.data !== null) {
        newOrderFunc(resp.data.data)
      }
    })
      .catch(err => {
        setError({
          title: 'Shipping error',
          message: err.response.data.message || 'Server error'
        })
        setShowError(true)

        setLoading(false)
      })
  }
  const next = () => {
    if (state === 2) {
      setLoading(true)
      userApi('POST', values, 'shipments/cost')
        .then(resp => {
          if (resp.data.data.cost) {
            const v = values
            v.cost = resp.data.data.cost
            v.email = resp.data.data.email
            setValues(v)
            setState(state + 1)
            setLoading(false)
          }
        })
        .catch(err => {
          setShowError(true)
          setError({
            title: 'Shipping error',
            message: err.response.data.message || 'Server error'
          })
          setLoading(false)
        })
    } else {
      setState(state + 1)
    }
  }
  /**
   * Hols information about different states.
   * It has the text which is the label of the input field
   * inputType which is the type of input the input should accept
   * type which is the name in the values the given input will have
   * validation, the validation function used to validate the input.
   */
  const stateType = {
    0: [
      {
        text: 'Vart vill du skicka ditt paket?',
        inputType: 'text',
        type: 'destination',
        validation: (value) => true
      },
      {
        text: 'Vad heter mottagaren?',
        inputType: 'text',
        type: 'receiver',
        validation: (value) => value.length > 1
      }],
    1: [
      {
        text: 'Vad väger ditt paket? (Kg)',
        inputType: 'number',
        type: 'weight',
        validation: (value) => value > 0
      },
      {
        text: 'Vad har du för mail?',
        inputType: 'text',
        type: 'email',
        hide: hideEmail,
        validation: (value) => validateEmail(value)
      }
    ],
    2: [
      {
        text: 'Vad vill du ha för färg på ditt paket?',
        inputType: 'color',
        type: 'boxColor',
        validation: (value) => true
      }
    ]
  }

  if (loading) {
    return (
      <div className={styles.innerContainer}>
        <Logo />
        <div className={styles.inputContainer}>
          <Loader />
        </div>
      </div>
    )
  }
  if (state < 3) {
    return (
      <>
        <Logo />
        <div className={styles.innerContainer}>
          <Alerter show={showError} title={error.title} message={error.message} hide={() => setShowError(false)} />
          <div className={styles.inputContainer}>
            {stateType[state].map(stateProps => stateProps.hide == null && <InputWrapper key={stateProps.type} title={stateProps.text} type={stateProps.inputType} validator={stateProps.validation} inputFunc={(h) => setNewValue(stateProps.type, h)} defaultValue={values[stateProps.type]} />)}
          </div>
          <div className={styles.buttonContainer}>
            {state > 0 ? <Button title='Bakåt' onClickFunction={() => setState(state - 1)} /> : <div />}
            <Button title='Nästa' onClickFunction={next} />
          </div>
        </div>
      </>
    )
  }
  // Confirming the information
  else if (state === 3) {
    return (
      <>
        <div className={styles.innerContainerConfirmationTable}>
          <Alerter show={showError} title={error.title} message={error.message} hide={() => setShowError(false)} />

          <ConfirmInformationTable email={values.email} weight={values.weight} receiver={values.receiver} color={values.boxColor} destination={values.destination} cost={values.cost} />
          <div className={styles.buttonContainer}>
            <Button title='Bakåt' onClickFunction={() => setState(state - 1)} />
            <Button title='Skicka' onClickFunction={sendOrder} />
          </div>
        </div>
      </>
    )
  }
  // Check auth
  else {
    if (hideEmail == null) {
      return (<VerificationSentContainer email={values.email} reset={reset} />)
    } else {
      return (<OrderSentContainer reset={reset} />)
    }
  }
}
