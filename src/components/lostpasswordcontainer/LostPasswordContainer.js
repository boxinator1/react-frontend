import React, { useState } from 'react'
import { validateEmail } from '../../utils/utils'
import Logo from '../logo/Logo'
import InputWrapper from '../inputwrapper/InputWrapper'
import Button from '../button/Button'
import styles from './LostPasswordContainer.module.css'
import Loader from '../loader/Loader'
import Alerter from '../alerter/Alerter'
import userApi from '../../services/userapi'
import { useHistory } from 'react-router-dom'

/**
 * This container handels all the different flows when sending a package.
 * In the stateType you can see the first input types for the first three states.
 * Each of the inputs append information to our values state.
 * After accepting the information the user will be send to either {@link ../ordersentcontainer/OrderSentContainer} or {@link ../verificationsentcontainer/VerificationSentContainer}
 * depending on wether or not they are authenticated.
 */
export default function LostPasswordContainer ({ hideEmail = null }) {
  const history = useHistory()
  const [state, setState] = useState(0)
  const [email, setEmail] = useState(null)
  const [loading, setLoading] = useState(false)
  const [code, setCode] = useState(null)
  const [password, setPassword] = useState(null)
  const [confirmPassword, setConfirmPassword] = useState(null)
  const [showError, setShowError] = useState(false)
  const [error, setError] = useState({
    title: '',
    message: ''
  })

  const next = () => {
    setLoading(true)
    switch (state) {
      case 0:
        if (email != null) {
          userApi('POST', email, 'account/lost')
            .then((resp) => {
              if (resp.status === 200) {
                setError({
                  title: 'Information',
                  message: 'Denna emailen har redan en verifikation som väntar.. Var vänlig kontrollera din email.'
                })
              }
              else {
                setError({
                  title: 'Information',
                  message: 'En verifikationskod har blivit skickad till din email.'
                })
              }
              setState(state + 1)
            })
            .catch(err => {
              let message = 'Server fel'
              if (err.response.status) {
                if (err.response.status === 404) {
                  message = 'Emailen hittades ej'
                }
              }

              setError({
                title: 'Kunde ej skicka kod',
                message: err.response.data.message || message
              })
              
            })
            .finally(() => {
              setShowError(true)
              setLoading(false)
            })
        } else {
          setError({
            title: 'Email',
            message: 'Du måste ange en email i korrekt format.'
          })
          setShowError(true)
          setLoading(false)
        }
        break
      case 1:
        if (password != null && confirmPassword != null && code != null) {
          const body = {
            password,
            email,
            code
          }
          userApi('POST', body, 'account/newPassword')
            .then(resp => {
              setState(state + 1)
              setError({
                title: 'Information',
                message: 'Ditt lösenord har uppdaterats.. Du blir vidare skickad till login inom 5 sekunder.'
              })
              setTimeout(() => history.replace('/login'), 5000)
            })
            .catch(err => {
              setError({
                title: 'Fel',
                message: err.response.data.message || 'Server fel'
              })
            })
            .finally(() => {
              setShowError(true)
              setLoading(false)
            })
        } else {
          setError({
            title: 'Inskrivet',
            message: 'Du måste ange korrekta värden.'
          })
          setShowError(true)
        }
        break
      default:
          
    }
  }

  const stateRendering = () => {
    switch (state) {
      case 0:
        return (<>
          <p>Var vänlig ange den email address som är kopplat till det kontot du vill ha ett nytt lösenord till.</p>
          <InputWrapper title='Din email address' validator={(nEmail) => validateEmail(nEmail)} inputFunc={(nEmail) => setEmail(nEmail)} />
                </>)
      case 1:
        return (<>
          <p>Var vänligen ange koden som skickades till din mail. <b> Din kod kommer fungera i 60 minuter </b> Ange även ditt nya lösenord samt en identisk kopia</p>
          <></>
          <InputWrapper title='Kod från din email' type='text' validator={(nCode) => nCode.length > 5} inputFunc={(nCode) => setCode(nCode)} />
          <InputWrapper title='Ditt nya lösenord' type='password' validator={(nPassword) => nPassword.length > 6} inputFunc={(nPassword) => setPassword(nPassword)} />
          <InputWrapper title='Bekräfta ditt lösenord' type='password' validator={(cPassword) => password.length > 6 && cPassword === password} inputFunc={(cPassword) => setConfirmPassword(cPassword)} />
        </>)
      default:
    }
  }

  if (loading) {
    return (
      <div className={styles.innerContainer}>
        <Logo />
        <div className={styles.inputContainer}>
          <Loader />
        </div>
      </div>
    )
  }
  if (state < 3) {
    return (
      <div className={styles.innerContainer}>
        <Alerter show={showError} title={error.title} message={error.message} hide={() => setShowError(false)} disableHide={state === 2} />
        <Logo />
        <div className={styles.inputContainer}>
          {stateRendering()}
        </div>
        <div className={styles.buttonContainer}>
          <div />
          <Button title={state > 0 ? 'Spara' : 'Skicka'} onClickFunction={next} />
        </div>
      </div>
    )
  }
}
