import React from 'react'
import PropTypes from 'prop-types';

//Styling
import styles from './ModalWrapper.module.css'


// ModalWrapper that takes a boolean prop displayModal
class ModalWrapper extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false
        }
    }

    closeModal = (event) => {
        event.preventDefault();
        if (event.currentTarget === event.target) {
            this.props.setModal(false)
            this.setState({
                showModal : false
            })
        }
    }

    componentDidMount() {
        this.setState({
            showModal: this.props.displayModal
        })
    }


    render() {
        const { showModal } = this.state;
        const { props } = this;
        return( 
            <React.Fragment>
            { showModal ? 
             <div onClick={ e => this.closeModal(e)} className={styles.modal_wrapper}>
                {React.cloneElement(props.children, {className: styles.modal_content} )}
            </div> : null
            
            }
            
            </React.Fragment>
        )
    }
}

ModalWrapper.propTypes = {
    displayModal: PropTypes.bool.isRequired
}

export default ModalWrapper;

