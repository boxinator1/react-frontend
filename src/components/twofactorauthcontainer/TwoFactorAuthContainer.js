import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { logIn } from '../../services/AuthService'
import TwoFactorAuthInput from '../twofactorauthinput/TwoFactorAuthInput'
import TwoFactorAuthTimer from '../twofactorauthtimer/TwoFactorAuthTimer'
import userApi from '../../services/userapi'
import styles from './TwoFactorAuthContainer.module.css'
export default function TwoFactorAuthContainer ({ email }) {
  const history = useHistory()
  const [code, setCode] = useState('0000')
  const [error, setError] = useState(false)

  const setNumber = (value, pos) => {
    const newCode = code.substring(0, pos) + value + code.substring(pos + 1)
    setCode(newCode)
    if (pos === 3) {
      userApi('POST', { email, code: newCode }, 'login/verify')
        .then(resp => {
          logIn(resp.data.data.token, resp.data.data.type)
          history.push('/')
        })
        .catch(() => {
          setCode('0000')
          document.activeElement.blur()
          setError(true)
          setTimeout(() => {
            setError(false)
          }, 500)
        })
    }
  }

  return (
    <div className={styles.container}>
      <p>
          För att authenticera dig vänligen skriv in koden som skickades till {email}
      </p>
      <div className={styles.inputContainer + ' ' + (error ? styles.shaker : '')}>
        <TwoFactorAuthInput passInput={(value) => setNumber(value, 0)} reset={error} />
        <TwoFactorAuthInput passInput={(value) => setNumber(value, 1)} reset={error} />
        <TwoFactorAuthInput passInput={(value) => setNumber(value, 2)} reset={error} />
        <TwoFactorAuthInput passInput={(value) => setNumber(value, 3)} reset={error} />
      </div>
      <div className={styles.timerAndButton}>
        <TwoFactorAuthTimer />
      </div>

    </div>
  )
}
