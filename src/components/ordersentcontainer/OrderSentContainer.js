import React from 'react'
import Logo from '../logo/Logo'
import styles from './OrderSentContainer.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRedo } from '@fortawesome/free-solid-svg-icons'
export default function OrderSentContainer ({ reset }) {
  return (
    <div className={styles.innerContainer}>
      <Logo />
      <p className={styles.bigText}>Din order har mottagits!<br /> Tack för att du valde Boxinator</p>
      <div className={styles.iconHolder}>
        <FontAwesomeIcon icon={faRedo} size='4x' className={styles.fa} onClick={reset || null} />
        <label>Ny Order</label>
      </div>

      {!localStorage.getItem('authKey') && <p className={styles.smallerText}>Psst, om du vill se alla din ordrar och deras status så kan du skapa en användare uppe till vänster!</p>}
    </div>
  )
}
