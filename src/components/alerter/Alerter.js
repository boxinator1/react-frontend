import React from 'react'
import styles from './Alerter.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
export default function Alerter ({ title, message, show, hide, disableHide = false }) {
  if (show) {
    return (
      <div className={styles.container} onClick={!disableHide ? hide : null}>
        <div className={styles.alerter}>
          {!disableHide && <FontAwesomeIcon onClick={hide} className={styles.fontAwesomePosition} icon={faTimesCircle} />}
          <h1>{title}</h1>
          <p>{message}</p>

        </div>
      </div>
    )
  }
  return null
}
