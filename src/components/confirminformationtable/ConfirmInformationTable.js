import React from 'react'
import styles from './ConfirmInformationTable.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBox } from '@fortawesome/free-solid-svg-icons'

export default function ConfirmInformationTable ({ destination, receiver, weight, email, color, cost }) {
  return (
    <table className={styles.infoBox}>
      <tbody>
        <tr>
          <td>Destination:</td>
          <td>{destination}</td>
        </tr>
        <tr>
          <td>Mottagare:</td>
          <td>{receiver}</td>
        </tr>
        <tr>
          <td>Vikt:</td>
          <td>{weight}kg</td>
        </tr>
        <tr>
          <td>Email:</td>
          <td>{email}</td>
        </tr>
        <tr>
          <td>Paketfärg:</td>
          <td><FontAwesomeIcon icon={faBox} style={{color: color, fontSize: '1.5em'}}/></td>
        </tr>
        <tr>
          <td>Kostnad:</td>
          <td>{cost} SEK</td>
        </tr>
      </tbody>
    </table>
  )
}
