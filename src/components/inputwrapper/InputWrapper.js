import React, { useState } from 'react'
import Input from '../input/Input'
import './InputWrapper.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import Popover from '../popover/Popover'
/**
 * If validator fails the input will be null.
 * @param title - The title of the input
 * @param type - Type of the input
 * @param inputFunc - The function which should be executed if the validator passes the value
 * @param validator - The function which validates the input.
 */
export default function InputWrapper ({ title, type = 'text', inputFunc, validator = null, defaultValue = '', validationMsgs = null }) {
  const [valid, setValid] = useState(false)
  const [init, setInit] = useState(false)
  const [display, setDisplay] = useState(false)
  const checkValidation = (value) => {
    if (validator != null) {
      setInit(true)
      if (validator(value)) {
        setValid(true)
        inputFunc(value)
      } else {
        setValid(false)
        inputFunc(null)
      }
    } else {
      inputFunc(value)
    }
  }
  return (
    <div className='inputWrapper'>
      <Input title={title} type={type} inputFunc={checkValidation} defaultValue={defaultValue || ''} onFocusTrigger={() => setDisplay(true)} onBlurTrigger={() => setDisplay(false)} />
      {init && type !== 'date' &&
        <span className='absolut'>
          {valid
            ? <FontAwesomeIcon icon={faCheck} style={{ color: 'green' }} />
            : <FontAwesomeIcon style={{ color: 'red' }} icon={faTimes} />}
        </span>}
      {validationMsgs && <Popover display={display} messages={validationMsgs} />}
    </div>
  )
}
