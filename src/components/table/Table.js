import React, { useState, useEffect } from 'react'
import { useTable, usePagination } from 'react-table'
import userApi from '../../services/userapi'
import { logOut, isAdmin } from '../../services/AuthService'
// Styling
import styles from './Table.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBackward, faCaretLeft, faForward, faCaretRight, faTimesCircle, faBox } from '@fortawesome/free-solid-svg-icons'
import score from 'string-score'
// Components
import ModalWrapper from '../modalwrapper/ModalWrapper'
import Button from '../button/Button'
import Input from '../input/Input'

import SendPackageContainer from '../sendpackagecontainer/SendPackageContainer'
import Alerter from '../alerter/Alerter'
import Loader from '../loader/Loader'
  /**
   * Only works up to 100k
   * @param {} kilo 
   */
  const renderSekNicely = (sek) => {
    const position  = sek.length -3
    if (position < 1) {
      return sek
    }
    let first = sek.substring(0,position)
    let second = sek.substring(position)
    return first + " " + second
  }
// React-table component takes two option objects with columns and the data to be displayed
export const Table = ({ columns, data, createNewCountry, settingsChangeFunc, settingsMultFunc, countryId, typeOfTable, newCountryFunc, NewMultFunc, deleteCountryFunc, modalOutgoing, editData, onClicker, setOnClicker }) => {
  const [displayModal, setDisplayModal] = useState(false)
  const [status, setStatus] = useState(1)
  const [orderData, setOrderData] = useState({})
  const [modalChoice, setModalChoice] = useState(typeOfTable || 0)
  const [showAlert, setShowAlert] = useState(false)
  const [loading, setLoading] = useState(false)
  const [showingUser, setShowingUser] = useState(false)
  const [tempOrderData, setTempOrderData] = useState({})
  const [alertInfo, setAlertInfo] = useState({
    title: '',
    message: ''
  })
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 }
    },
    usePagination
  )
  useEffect(() => {
    if (setOnClicker != null) {
      setOnClicker(newOrderClick)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  // Toggle the displayModal state and stores the data from the clicked row which is later passed to the
  // modalwrapper. Also scrollTo top if the user has expanded the number of rows that are displayed.
  const tableRow = (rowData) => {
    modalOutgoing === 2 && countryId(rowData.original) // Inserts the correct ID to the countrySettingsView.js
    setModalChoice(1)
    setDisplayModal(prevState => !prevState)
    setOrderData(rowData.original)
    window.scrollTo(0, 0)
  }
  const cancelOrderClick = () => {
    if (window.confirm('Är du säker att du vill avbryta denna ordern?')) {
      userApi('PATCH', null, 'shipments/' + orderData.id)
        .then(resp => {
          setAlertInfo({
            title: 'Order uppdaterad',
            message: 'Du har avbrytit din order'
          })
          editData(orderData.id, 5)
        })
        .catch(err => {
          setAlertInfo({
            title: 'Order uppdatering misslyckad',
            message: err.response.data.message || 'Server fel'
          })
        })
        .finally(() => {
          setLoading(false)
          setShowAlert(true)
        })
    }
  }
  const orderStatusToSwedish = (status) => {
    switch (status) {
      case 'PENDING':
        return 'Obekräftad'
      case 'CREATED':
        return 'Skapad'
      case 'RECEIVED':
        return 'Mottagen'
      case 'INTRANSIT':
        return 'Påväg'
      case 'COMPLETED':
        return 'Levererad'
      case 'CANCELLED':
        return 'Avbruten'
      default:
        return 'Unknown'
    }
  }
  const getUserToShow = () => {
    setShowingUser(true)
    setLoading(true)

    userApi('GET', null, 'account/email/' + orderData.email)
      .then(resp => {
        setTempOrderData(orderData)
        const data = resp.data.data;
        data.dateOfBirth = data.dateOfBirth.split('T')[0];
        setOrderData(data)
      })
      .catch(er => {
        setAlertInfo({
          title: 'Misslyckades att hämta användare',
          message: er.response.data.message | 'Server fel'
        })
        setShowAlert(true)
      })
      .finally(setLoading(false))
  }
  const backFromUserModal = () => {
    setOrderData(tempOrderData)
    setShowingUser(false)
  }
  const deleteUser = () => {
    if (showingUser) {
      if (window.confirm('Vill du verkligen ta bort denna användaren?')) {
        setLoading(true)
        userApi('DELETE', null, 'account/' + orderData.id)
          .then(resp => {
            setAlertInfo({
              title: 'Användare borttagen',
              message: 'Du har tagit bort användaren'
            })
          })
          .catch(er => {
            setAlertInfo({
              title: 'Fel vid borttaggning',
              message: er.response.data.message | 'Server fel'
            })
          })
          .finally(() => {
            editData(0, null, false, orderData.email)
            setLoading(false)
            setDisplayModal(false)
            setShowingUser(false)
            setOrderData({})
            setShowAlert(true)
          })
      }
    }
  }

  const changeStatusClick = () => {
    setLoading(true)
    userApi('PATCH', status, 'shipments/' + orderData.id)
      .then(resp => {
        setAlertInfo({
          title: 'Order uppdaterad',
          message: 'Du har uppdaterad ordern'
        })
        editData(orderData.id, status)
      })
      .catch(err => {
        setAlertInfo({
          title: 'Order uppdatering misslyckad',
          message: err.response.data.message || 'Server fel'
        })
      })
      .finally(() => {
        setLoading(false)
        setShowAlert(true)
      })
  }
  const keyToSwedish = (key) => {
    switch(key) {
      case 'id':
        return 'Paket-Id'
      case 'receiver':
        return 'Mottagare'
      case 'status':
        return 'Order Status'
      case 'weight':
        return 'Vikt'
      case 'boxColor':
        return 'Paketfärg'
      case 'date':
        return 'Skapad'
      case 'cost':
        return 'kostnad'
      default: 
        return key
    }
  }
  const delteOrderClick = () => {
    if (window.confirm('Är du säker att du vill ta bort denna ordern?')) {
      setLoading(true)
      userApi('DELETE', null, 'shipments/' + orderData.id)
        .then(resp => {
          setAlertInfo({
            title: 'Order bortaggen',
            message: 'Du har tagit bort ordern'
          })
          editData(orderData.id, null, true)
        })
        .catch(err => {
          setAlertInfo({
            title: 'Order borttagning misslyckad',
            message: err.response.data.message || 'Server fel'
          })
        })
        .finally(() => {
          setLoading(false)
          setShowAlert(true)
          setDisplayModal(false)
        })
    }
  }

  const closeModal = () => {
    setDisplayModal(false)
    setShowingUser(false)
  }
  const newOrderClick = () => {
    setModalChoice(0)
    setDisplayModal(prevState => !prevState)
  }
  const newCountryClick = () => {
    setModalChoice(2)
    setDisplayModal(prevState => !prevState)
  }

  const countryDelete = () => {
    deleteCountryFunc()
    setDisplayModal(prevState => !prevState)
  }
  const countryPatch = () => {
    settingsChangeFunc()
    setDisplayModal(prevState => !prevState)
  }
  const countryPost = () => {
    createNewCountry()
    setDisplayModal(prevState => !prevState)
  }
  if (loading && !displayModal) {
    return <Loader />
  }
  return (
    <>
      <Alerter show={showAlert} hide={() => setShowAlert(false)} title={alertInfo.title} message={alertInfo.message} />
      {
        typeOfTable === 2 && <Button title='Nytt Land' onClickFunction={newCountryClick} />
      }
      <table className={styles.table_style} {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()} onClick={e => tableRow(row)}>
                {row.cells.map(cell => {
                  if (cell.column.id === 'boxColor') {
                    return <td {...cell.getCellProps()}><FontAwesomeIcon icon={faBox} style={{color: cell.value, fontSize: '1.5em'}}/> </td>
                  }
                  return <td {...cell.getCellProps()}>{cell.column.id === 'cost' ? renderSekNicely(cell.value + "") : cell.column.id === 'status' ? orderStatusToSwedish(cell.value) : cell.render('Cell')} {cell.column.id === 'weight' && "kg"}{cell.column.id === 'cost' && "SEK" }</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
      {/*
            Pagination for the table with fontawesome icons and some styling
          */}
      <div className={styles.pagination}>
        <div className={styles.flex_item}>
          <button className={styles.icon_style} onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            <FontAwesomeIcon icon={faBackward} />
          </button>
          <button className={styles.icon_style} onClick={() => previousPage()} disabled={!canPreviousPage}>
            <FontAwesomeIcon icon={faCaretLeft} />
          </button>
          <button className={styles.icon_style} onClick={() => nextPage()} disabled={!canNextPage}>
            <FontAwesomeIcon icon={faCaretRight} />
          </button>
          <button className={styles.icon_style} onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            <FontAwesomeIcon icon={faForward} />
          </button>
        </div>
        <div className={styles.flex_item_two}>
          <span>
            Sida {' '}
            <strong>
              {pageIndex + 1} av {pageOptions.length}
            </strong>
          </span>
          <span>
            |
          </span>
          <span className={styles.page_span}>
            Gå till sida{' '}
            <input
              className={styles.table_input}
              type='number'
              min='1'
              max={pageOptions.length}
              defaultValue={pageIndex + 1}
              onChange={e => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
              style={{ width: '100px' }}
            />
          </span>{' '}
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Visa {pageSize}
              </option>
            ))}
          </select>
        </div>

      </div>

      {
        /* When displayModal is true the modalwrapper is displayed with data from the clicked row */
        displayModal
          ? <ModalWrapper displayModal={displayModal} setModal={setDisplayModal} ownStyle={modalChoice === 0}>
            {modalChoice === 1
              ? <div>
                <FontAwesomeIcon onClick={closeModal} className={styles.close_button} icon={faTimesCircle} />
                {
                  Object.entries(orderData).map(([key, value], i) => {
                    let capitalizedKey = keyToSwedish(key)
                    capitalizedKey = capitalizedKey.charAt(0).toUpperCase() + capitalizedKey.slice(1)
                    return (
                      <React.Fragment key={i}>
                        {
                          <div key={i}>
                            <span> {capitalizedKey}:
                            </span>
                            {key === 'boxColor' ?
                              <FontAwesomeIcon icon={faBox} style={{color: value, fontSize: '1.5em'}}/>
                            :
                  
                            isAdmin() && key === 'email' && !showingUser
                              ? <p className={styles.textfield + ' ' + styles.hover} onClick={() => getUserToShow()}><u><b>{value} [Visa]</b></u></p>
                              : <p className={styles.textfield}>{key === 'cost' ? renderSekNicely(value + "") : key === 'status' ? orderStatusToSwedish(value) : value} {key === 'weight' && "kg"}{key === 'cost' && "SEK" }</p>}
                          </div>
                        }
                      </React.Fragment>
                    )
                  })
                }
                {modalOutgoing === 2
                  ? <>
                    <div className='knappInput' style={{ borderBottom: 'none', marginTop: '0', display:'inline-block' }}>
                      <Input title='Ändra multiplier' inputFunc={settingsMultFunc} />
                      <div className={styles.buttonGroupCountries} >
                        <Button className={styles.changeMult_button} onClickFunction={countryPatch} title='Spara' />
                        <Button className={styles.delete_button} onClickFunction={countryDelete} deleteColor title='Radera' />
                      </div>
                    </div>

                   
                  </>
                  : showingUser && isAdmin()
                    ? <div className='deleteKnapp' style={{ borderBottom: 'none' }}>
                      <Button className={styles.delete_button} onClickFunction={backFromUserModal} title='Bakåt' />
                      <Button className={styles.delete_button} onClickFunction={deleteUser} deleteColor title='Ta bort användare' />
                      </div>
                    : isAdmin()
                      ? <>
                        <div className={styles.selectButtonContainer} style={{ borderBottom: 'none' }}>
                          {!loading && <>
                            <select name='status' value={status} onChange={(event) => setStatus(event.target.value)}>
                              <option value='1'>Skapad</option>
                              <option value='2'>Mottagen</option>
                              <option value='3'>Påväg</option>
                              <option value='4'>Levererad</option>
                              <option value='5'>Avbruten</option>
                            </select>
                            <Button className={styles.delete_button} onClickFunction={changeStatusClick} title='Uppdatera status' />
                            <Button className={styles.delete_button} onClickFunction={delteOrderClick} deleteColor title='Ta bort order' />
                          </>}
                        </div>
                      </>
                      : <>
                        <div className='deleteKnapp' style={{ borderBottom: 'none' }}>
                          <Button className={styles.delete_button} onClickFunction={cancelOrderClick} title='Avbryt order' />
                        </div>
                      </>}

                </div>
              : modalChoice === 2 /* 2 för NYTT LAND KNAPP */

                ? <div>
                  <FontAwesomeIcon onClick={closeModal} className={styles.close_button} icon={faTimesCircle} />

                    <div className='landInput' style={{ borderBottom: 'none', marginTop: '5%', display: 'inline-block', maxWidth: '80%' }}>
                      <Input title='Land' inputFunc={newCountryFunc} />
                      <Input type="number" title='Multiplier' inputFunc={NewMultFunc} />

                      <div style={{ marginTop:'10%' }} >
                       <Button onClickFunction={countryPost} title='Lägg till' />
                      </div>
                    </div>

                  </div>

                : <>
                  <div className={styles.sendContainermodal}>
                    <FontAwesomeIcon onClick={closeModal} className={styles.fontAwesomePosition} icon={faTimesCircle} />
                    <div className={styles.sendContainerContainer}>
                      <SendPackageContainer hideEmail={0} newOrderFunc={(pack) => editData(null, null, null, null, pack)} />
                    </div>
                  </div>
                </>}

            </ModalWrapper> : null
      }
    </>
  )
}

// Sets the columns and the data that is passed to Table component
const TableData = () => {
  const [data, setData] = useState([])
  const [showFilter, setShowFilter] = useState(false)
  const [showData, setShowData] = useState([])
  const [email, setEmail] = useState('')
  const [oldest, setOldest] = useState(false)
  const [toggels, setToggels] = useState({
    PENDING: true,
    CREATED: true,
    RECEIVED: true,
    INTRANSIT: true,
    COMPLETED: true,
    CANCELLED: true
  })
  const [onClicker, setOnClicker] = useState({
    func: function () { console.log('hello') }
  })
  const [columns, setColumns] = useState([])
  const [loading, setLoading] = useState(true)
  const [totalData, setTotalData] = useState({
    cost: 0,
    weight: 0
  })
  const setColumnsF = () => {
    setColumns(

      () => window.innerWidth > 700 ? [

        {
          Header: 'Status',
          accessor: 'status'
        },
        {
          Header: 'Orderdatum',
          accessor: 'date'
        },
        {
          Header: 'Mottagare',
          accessor: 'receiver'
        },
        {
          Header: 'Destination',
          accessor: 'destination'
        },
        {
          Header: 'Vikt',
          accessor: 'weight'
        },
        {
          Header: 'Färg',
          accessor: 'boxColor'
        },
        {
          Header: 'Kostnad',
          accessor: 'cost'
        }
      ]
        : [
          {
            Header: 'Status',
            accessor: 'status'
          },
          {
            Header: 'Orderdatum',
            accessor: 'date'
          },
          {
            Header: 'Färg',
            accessor: 'boxColor'
          }
        ]
    )
  }
  useEffect(() => {
    setColumnsF()
    setLoading(true)
    userApi('GET', {}, 'shipments')
      .then((resp) => {
        const total = { cost: 0, weight: 0 }
        const data = resp.data.data.map(d => {
          d.date = d.date.split('T')[0]
          total.cost += d.cost
          total.weight += d.weight
          return d
        })
        setTotalData(total)
        setData(data)
        filterFunc(data)
      })
      .catch(err => {
        if (err.response) {
          if (err.response) {
            if (err.response.status === 401) {
              logOut()
            }
          }
        }
      })
      .finally(() => {
        setLoading(false)
      })
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])



  const filterFunc = (inData = null) => {
    let dat = null
    if (inData) {
      dat = inData
    } else {
      dat = data
    }
    if (email !== '') {
      dat = dat.filter(p => {
        return (p.email.includes(email) || score(email, p.email, 0.5) >= 0.1) && p.email.length >= email.length
      })
      dat.sort((a, b) => {
        const s = score(email, b.email, 0.5) - score(email, a.email, 0.5)
        if (s === 0) {
          return b.id - a.id
        }
        return s
      })
    }
    oldest ? dat.sort((b, a) => b.id - a.id) : dat.sort((a, b) => b.id - a.id)

    setShowData(dat.filter(p => toggels[p.status.toUpperCase()]))
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => filterFunc(), [oldest])
  const editData = (id, newStatus = null, deleted = false, userDeleted = '', addNew = null) => {
    let tempDat = null
    if (addNew !== null) {
      addNew.date = addNew.date.split('T')[0]
      const totals = totalData
      totals.cost += addNew.cost
      totals.weight += addNew.weight
      setTotalData(totals)
      tempDat = [addNew, ...data]
    } else if (deleted || userDeleted !== '') {
      tempDat = data.filter(d => {
        if (userDeleted !== '') {
          return d.email.toLowerCase() !== userDeleted.toLowerCase()
        } else {
          return d.id !== id
        }
      })
    } else {
      tempDat = data.map(d => {
        if (d.id === id) {
          if (newStatus != null) {
            const getStatus = (status) => {
              switch (Number(status)) {
                case 1:
                  return 'CREATED'
                case 2:
                  return 'RECEIVED'
                case 3:
                  return 'INTRANSIT'
                case 4:
                  return 'COMPLETED'
                case 5:
                  return 'CANCELLED'
                default:
                  return 'Unknown'
              }
            }
            d.status = getStatus(newStatus)
          }
        }
        return d
      })
    }
    setData(tempDat)
    filterFunc(tempDat)
  }
  window.addEventListener('resize', setColumnsF)
  if (loading) {
    return <Loader />
  }
  return (
    <>
    <div className={styles.hider}>
    <Button onClickFunction={() => setShowFilter(!showFilter)} title={(showFilter ? 'Dölj ' : 'Visa ') + 'filtrering'} />
    {showFilter && <>  
    {isAdmin() && <div className={styles.inputEmail}><Input  title="Sök på e-postadress" inputFunc={(email) => setEmail(email)} /></div> }
      <div className={styles.filterDiv}>
        
        <div className={styles.checkboxes} >
          
          <label>
        PENDING
            <input type='checkbox' checked={toggels.PENDING} onChange={(event) => setToggels({ ...toggels, PENDING: event.target.checked })} />
          </label>
          <label>
        CREATED
            <input type='checkbox' checked={toggels.CREATED} onChange={(event) => setToggels({ ...toggels, CREATED: event.target.checked })} />
          </label>
          <label>
        INTRANSIT
            <input type='checkbox' checked={toggels.INTRANSIT} onChange={(event) => setToggels({ ...toggels, INTRANSIT: event.target.checked })} />
          </label>
          <label>
        COMPLETED
            <input type='checkbox' checked={toggels.COMPLETED} onChange={(event) => setToggels({ ...toggels, COMPLETED: event.target.checked })} />
          </label>
          <label>
        CANCELLED
            <input type='checkbox' checked={toggels.CANCELLED} onChange={(event) => setToggels({ ...toggels, CANCELLED: event.target.checked })} />
          </label>
        </div>

        
        
      </div>
      <Button onClickFunction={() => filterFunc()} title='Filtrera' />
      </>}
      <div className={styles.sortAboveTable}>
        <label>
      Sortera efter {' '}
        <select onChange={(event) => 
          setOldest(event.target.value === '1')} >
          <option value='0'>Nyaste</option>
          <option value='1'>Äldsta</option>
        </select>
      </label>
      
        <p>{isAdmin() && '(Alla ordrar)'}</p>
        <p>Total vikt: <b>{totalData.weight.toFixed(1)} kg</b> </p>
        <p>Total kostnad: <b>{renderSekNicely(totalData.cost+"")} SEK</b> </p>
      
        <Button onClickFunction={() => onClicker.func()} title='Ny Order' />
      </div>
      </div>
      <div className={styles.sHider}>
        <Button onClickFunction={() => onClicker.func()} title='Ny Order' />
      </div>
      <Table columns={columns} data={showData} editData={editData} setOnClicker={(f) => setOnClicker({ func: f })} />
      
    </>
  )
}

export default TableData
