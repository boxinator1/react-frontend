import React, { useState, useEffect } from 'react'
import { SketchPicker } from 'react-color'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBox } from '@fortawesome/free-solid-svg-icons'
import './Input.css'

const Input = ({ name, title, type = 'text', inputFunc = null, defaultValue = '', onFocusTrigger = null, onBlurTrigger = null }) => {
  // Making a state for the input-field, whether it is empty or not.
  // It is checked on onBlur, meaning: active->not active

  const [blurred, setBlurred] = useState(defaultValue || type === 'password')
  const [input, setInput] = useState(defaultValue)
  const [displayColorPicker, setDisplayColorPicker] = useState(false)

  useEffect(() => {
    if (type === 'date') {
      const date = new Date(Date.parse(defaultValue))
      setInput(date.toLocaleDateString())
    }
    setInput(defaultValue)
    // eslint-disable-next-line
  }, [])

  const onBlur = (e) => {
    if (input.length < 1) {
      setBlurred(false)
    } else {
      setBlurred(true)
    }
    onBlurTrigger != null && onBlurTrigger()
  }
  /**
     * Changes the state input in the component and excetues the props inputFunc if it exists.
     * @param {*} event
     */
  const onInput = (event) => {
    defaultValue =
    setInput(event.target.value)
    if (inputFunc != null) {
      inputFunc(event.target.value)
    }
  }

  const handleColorPicker = (event) => {
    defaultValue = setInput(event.hex)
    if (inputFunc != null) {
      inputFunc(event.hex)
    }
  }

  const closeSketchPicker = () => {
    setDisplayColorPicker(false)
  }

  /**
     * Render with different classes if it is of type password.
     */
  switch (type) {
    case 'date':
      return (
        <div className='field'>
          {/* Input checks the state on onBlur */}
          <input className={blurred ? '' : 'dateBlurred'} value={input} name={name} type={type} onBlur={onBlur} onFocus={onFocusTrigger || null} onChange={(event) => onInput(event)} />
          {/* The label is tranformed when input is active and fixed in position when blurred==true */}
          <label className={blurred ? 'blurredClass' : 'notBlurredClass'}>{title}</label>
        </div>
      )
    case 'color':
      return (
        <div className='field_sketchpicker'>
          {/* Input checks the state on onBlur */}
          <label style={{ alignSelf: 'center' }}>{title}</label>
          <FontAwesomeIcon onClick={() => setDisplayColorPicker(prevState => !prevState)} icon={faBox} style={{color: input, fontSize: '2.5em', margin: '1em'}}/>
          {displayColorPicker ? <> <div onClick={closeSketchPicker} className='sketch-background' /> <SketchPicker className='sketchpicker' color={input} onChange={handleColorPicker} /> </> : null}

          {/* The label is tranformed when input is active and fixed in position when blurred==true */}

        </div>
      )
    default:
      return (
        <div className='field'>
          {/* Input checks the state on onBlur */}
          <input type={type} onBlur={onBlur} onChange={onInput} onFocus={onFocusTrigger || null} value={input} />
          {/* The label is tranformed when input is active and fixed in position when blurred==true */}
          <label className={blurred ? 'blurredClass' : 'notBlurredClass'}>{title}</label>
        </div>
      )
  }
}

export default Input
