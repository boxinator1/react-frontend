import React from 'react'
import styles from './Loader.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
export default function Loader () {
  return (
    <div className={styles.container}>
      <FontAwesomeIcon className={styles.icon} icon={faSpinner} />
    </div>
  )
}
