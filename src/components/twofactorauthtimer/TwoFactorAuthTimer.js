import React, { useState } from 'react'
import styles from './TwoFactorAuthTimer.module.css'
export default function TwoFactorAuthTimer () {
  const [createdTime, setCreatedTime] = useState(null)
  const [time, setTime] = useState(90)
  if (createdTime == null) {
    setCreatedTime(Date.now())
  }
  // To countdown when the window is minimized or changed.
  setTimeout(function () {
    setTime(Math.floor(((createdTime + (60 * 1000)) - Date.now()) / 1000))
  }, 1000)
  if (time === 0 || (createdTime != null && (createdTime + (60 * 1000) < Date.now()))) {
    document.location.reload()
  }
  return (
    <div className={styles.container}>
      <div className={styles.roundedTimer} />
      <span className={styles.text}>{time}</span>
    </div>
  )
}
