import React, { useState, useEffect } from 'react'
import styles from './Popover.module.css'

export default function Popover ({ messages, display = false }) {
  const [re, setRe] = useState(false)

  useEffect((v) => {
    if (display) {
      setTimeout(() => {
        setRe(true)
      }, 10)
    } else {
      setRe(false)
    }
  }, [display])
  if (display) {
    return (
      <div className={re ? styles.container : styles.hidden}>
        {messages && messages.map((message, i) => <li key={i}>{message}</li>)}
      </div>
    )
  } else {
    return null
  }
}
