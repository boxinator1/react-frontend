import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import userApi from '../../services/userapi'
import Button from '../button/Button'
import Input from '../input/Input'
import { validateEmail } from '../../utils/utils'
import { logIn } from '../../services/AuthService'
import styles from './LoginContainer.module.css'
import InputWrapper from '../inputwrapper/InputWrapper'
import Alerter from '../alerter/Alerter'
import Loader from '../loader/Loader'
export default function LoginContainer ({ passEmailFunc, verified }) {
  const history = useHistory()
  const [email, setEmail] = useState(null)
  const [password, setPassword] = useState('null')
  const [showError, setShowError] = useState(false)
  const [errorInfo, setErrorInfo] = useState({
    message: '',
    title: ''
  })
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (verified) {
      setShowError(true)
      setErrorInfo({
        message: 'Din email har blivit verifierad, var vänligen logga in.',
        title: 'Lyckad verifiering'
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const onSubmit = () => {
    setLoading(true)
    if (email != null && password.length > 2) {
      userApi('POST', { email, password }, 'login').then(resp => {
        if (!resp.data.message) {
          logIn(resp.data.data.token, resp.data.data.type)
          history.push('/')
        } else {
          passEmailFunc(resp.data.data)
        }
        setLoading(false)
      })
        .catch(err => {
          if (err.response) {
            try {
              if (err.response.data.message != null) {
                setErrorInfo({
                  message: err.response.data.message,
                  title: 'Inloggningsfel'
                })
              } else {
                setErrorInfo({
                  message: 'Fel kombination av lösenord och email.',
                  title: 'Inloggningsfel'
                })
              }
            } catch (sErr) {
              setErrorInfo({
                message: 'Var vänligen försök igen lite senare.',
                title: 'Serverfel'
              })
            }
          } else {
            setErrorInfo({
              message: 'Server error',
              title: 'Inloggningsfel'
            })
          }
          setShowError(true)
          setLoading(false)
        })
    } else {
      setErrorInfo({
        message: 'Båda fieldsen måste ha validerat input.',
        title: 'Inloggningsfel'
      })
      setShowError(true)
      setLoading(false)
    }
  }
  if (loading) {
    return (
      <Loader />
    )
  }
  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <Alerter show={showError} message={errorInfo.message} title={errorInfo.title} hide={() => setShowError(false)} />
        <InputWrapper title='E-postadress' inputFunc={setEmail} validator={validateEmail} defaultValue={email} />
        <Input title='Lösenord' type='Password' inputFunc={setPassword} />
        <Link to='/lost'><span className={styles.link}>Glömt lösenord</span></Link>
      </div>
      <div className={styles.bottomHolder}>
        <Link to='/account'><span className={styles.link}>Registrera konto</span></Link>
        
        <Button title='Logga in' onClickFunction={onSubmit} />
      </div>
    </div>
  )
}
