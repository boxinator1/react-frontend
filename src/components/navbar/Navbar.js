import React from 'react'
import { NavLink } from 'react-router-dom';
import { isLoggedIn, isAdmin, logOut } from '../../services/AuthService'
// Icons and styling
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import styles from './Navbar.module.css'
import logo from '../../assets/logo/logo-mini.png'


class Navbar extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            displayDropdown: false,
        }
    }

    dropDown = () => {
        this.setState(prevState => ({
            displayDropdown: !prevState.displayDropdown
        }));
    }

    render() {
        const { displayDropdown } = this.state;

        return (

            <React.Fragment>
                <nav className={styles.nav}>
                    <NavLink to="/" className={styles.navlink}  ><img className={styles.img_style} src={logo} alt="Logo" /></NavLink>
                    <ul>
                        {isLoggedIn() ?
                            /*  @Media screen that enables these two buttons if screen is large. Also hides the cog-wheel. */
                            <>
                                {/* eslint-disable-next-line*/}
                                <li><a onClick={() => logOut()} className={styles.navlogin + ' ' + styles.logout} >Logga ut</a> </li>
                                <li><NavLink to="/edituser" className={styles.navlogin} activeStyle={{ color: 'white' }} >Min profil</NavLink> </li>
                                {isAdmin() ?
                                    <>
                                        <li><NavLink to="/main" className={styles.navlogin} activeStyle={{ color: 'white' }} >Alla ordrar</NavLink> </li>
                                        <li><NavLink to="/countrysettings" className={styles.navlogin} activeStyle={{ color: 'white' }} >Alla länder</NavLink> </li>
                                    </>
                                    :
                                    <li><NavLink to="/main" className={styles.navlogin} activeStyle={{ color: 'white' }} >Mina ordrar</NavLink> </li>
                                }
                            </>
                            :
                            <>
                                <li><NavLink to="/login" className={styles.navlogin} activeStyle={{ color: 'white' }} >Logga in</NavLink> </li>
                                <li><NavLink to="/account" className={styles.navlogin} activeStyle={{ color: 'white' }} >Registrera</NavLink> </li>
                            </>
                        }
                    </ul>
                    {/* Cog/Bars to the right, has onClick to displayDropdown*/}
                    <FontAwesomeIcon size="lg" icon={displayDropdown ? faTimes : faBars} className={styles.fa_bars} onClick={this.dropDown} />
                </nav>
                { displayDropdown ?
                    <>
                        <div className={styles.hiddenDiv} onClick={this.dropDown} />

                        <div className={styles.dropdown}>
                            {isLoggedIn() ?
                                <>
                                    <p> <NavLink to="/edituser" onClick={this.dropDown} >Min profil</NavLink></p>
                                    {isAdmin() ?
                                        <>
                                            <p> <NavLink to="/main" onClick={this.dropDown} >Alla ordrar</NavLink></p>
                                            <p> <NavLink to="/countrysettings" onClick={this.dropDown} >Alla länder</NavLink></p>
                                            <p onClick={() => logOut()} >Logga ut</p>
                                        </>
                                        :
                                        <>
                                            <p> <NavLink to="/main" onClick={this.dropDown} >Dina ordrar </NavLink></p>
                                            <p onClick={() => logOut()}  >Logga ut</p>
                                        </>
                                    }
                                </>
                                :
                                <>
                                    <p><NavLink to="/login" onClick={this.dropDown} >Logga in </NavLink></p>
                                    <p><NavLink to="/account" onClick={this.dropDown} >Registrera </NavLink></p>
                                </>
                            }
                        </div>
                    </>
                    :
                    null
                }
            </React.Fragment>
        )
    }
}

export default Navbar;
