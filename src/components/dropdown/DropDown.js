import React from 'react';
import PropTypes from 'prop-types';

// Styling
import styles from './DropDown.module.css'

// Simple dropdown component that renders a styled dropdown
class DropDown extends React.Component {
    constructor(props) {
        super(props);
    }

    handleStatusChange = (event) => {
        event.preventDefault();
    }
    render() {
        const {options} = this.props;
        return (
            <form className={styles.dropDown}>
            <select className={styles.select_el} onChange={this.handleStatusChange}> 
            {
                options.map( (option, i) => {
                return <option value={option} key={i}>{option}</option> 
                } )
            }
            </select> 
            </form>
        )
    }
}

DropDown.propTypes = {
    options: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default DropDown;