import React from 'react'
import PropTypes from 'prop-types'

import './Button.css'

// Button component that takes a title and an click event as props.
class Button extends React.Component {
  render () {
    const { title, onClickFunction, deleteColor } = this.props

    return (
      <button style={{ backgroundColor: deleteColor ? '#DC004E' : '' }} className='button-style' onClick={onClickFunction}>{title}</button>
    )
  }
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  onClickFunction: PropTypes.func.isRequired
}

export default Button
