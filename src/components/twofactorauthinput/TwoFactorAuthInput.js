import React, { useState, useEffect } from 'react'
import styles from './TwoFactorAuthInput.module.css'
export default function TwoFactorAuthInput ({ passInput, reset }) {
  const [input, setInput] = useState('')
  useEffect(() => {
    if (reset) {
      setInput('')
    }
  }, [reset])
  const onInputHandler = (e) => {
    const value = e.target.value
    if (value.length < 2 && !isNaN(Number(value))) {
      setInput(value)
      passInput(value)
    }
  }
  return (
    <input className={styles.boxOutline} value={input} onChange={onInputHandler} />
  )
}
