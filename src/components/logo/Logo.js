import React from 'react'
import FullLogo from '../../assets/logo/logo-full.png'
/**
 * Requieres grid of flex to be centered.
 */
export default function Logo () {
  return (
    <img style={{ height: '10vw', minHeight: '150px', justifySelf: 'center' }} src={FullLogo} alt='Boxinator' />
  )
}
