import React, { useState, useEffect } from 'react'
import Button from '../button/Button'
import styles from './UserInfoContainer.module.css'
import InputWrapper from '../inputwrapper/InputWrapper'
import userApi from '../../services/userapi'
import Alerter from '../alerter/Alerter'
import Loader from '../loader/Loader'
import { useHistory } from 'react-router-dom'
/**
 * Containing input fields for all of the users info.
 * @param register - If it should be the register view or not.
 */
export default function UserInfoContainer ({ register = false }) {
  const history = useHistory()
  // Regex which validates the email
  // eslint-disable-next-line
  const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  const [error, setError] = useState(false)
  const [noHide, setNoHide] = useState(false)
  const [loading, setLoading] = useState(false)
  const [errorInput, setErrorInput] = useState({
    message: '',
    title: ''
  })
  const [info, setInfo] = useState({
    firstName: null,
    lastName: null,
    email: null,
    password: null,
    confirmPassword: null,
    dateOfBirth: null,
    countryOfResidence: null,
    zipCode: null,
    contactNumber: null
  })

  useEffect(() => {
    if (!register) {
      setLoading(true)
      userApi('GET', {}, 'account/current')
        .then(resp => {
          const nData = resp.data.data
          nData.dateOfBirth = nData.dateOfBirth.split('T')[0]
          nData.password = ''
          nData.confirmPassword = ''
          setInfo(resp.data.data)
          setLoading(false)
        })
        .catch(() => {
          history.push('/')
        })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const fieldChange = (name, value) => {
    const e = { ...info }
    e[name] = value
    setInfo(e)
  }

  const submit = () => {
    // eslint-disable-next-line
    for (const [key, value] of Object.entries(info)) {
      if (value === null) {
        setErrorInput({ message: 'All information måste vara ifylld.', title: (register ? 'Registrerings' : 'uppdaterings') + 'fel' })
        setError(true)
        return
      }
    }
    if (info.password !== '' && info.confirmPassword !== info.password) {
      setErrorInput({ message: 'Lösenorden måste vara lika', title: (register ? 'Registrerings' : 'uppdaterings') + ' fel' })
      setError(true)
      return
    }
    setLoading(true)
    if (register) {
      registerSubmit()
    } else {
      updateSubmit()
    }
  }
  const updateSubmit = () => {
    const body = { ...info }
    userApi('PATCH', body, 'account/current')
      .then(resp => {
        const errorInputs = {
          message: 'Ditt konto har blivit uppdaterat!',
          title: 'Lyckad uppdatering'
        }
        setErrorInput(errorInputs)
        setError(true)
        setLoading(false)
      })
      .catch(err => {
        if (err.response) {
          const errorInputs = {
            message: err.response.data.message,
            title: 'Uppdaterings fel'
          }
          setErrorInput(errorInputs)
          setError(true)
          setLoading(false)
        }
      })
  }
  const registerSubmit = () => {
    const body = { ...info }
    userApi('POST', body, 'account')
      .then(resp => {
        const errorInputs = {
          message: 'Ditt konto har blivit skapat, var vänligen verifiera det genom att klicka på länknen i din email. Du kommer bli skickad till inloggningen inom 5 sekunder.',
          title: 'Lyckad registrering'
        }
        setErrorInput(errorInputs)
        setNoHide(true)
        setError(true)
        setTimeout(() => {
          window.location = '/login'
        }, 5000)
        setLoading(false)
      })
      .catch(err => {
        if (err.response) {
          const errorInputs = {
            message: err.response.data.message,
            title: 'Registrerings fel'
          }
          setErrorInput(errorInputs)
          setError(true)
          setLoading(false)
        }
      })
  }
  if (loading) {
    return (
      <Loader />
    )
  }
  return (
    <div className={styles.userInfoContainer}>
      <Alerter show={error} hide={() => setError(false)} title={errorInput.title} message={errorInput.message} disableHide={noHide} />
      <InputWrapper
        title='Förnamn'
        inputFunc={(v) => fieldChange('firstName', v)}
        validator={(value) => value.length > 1}
        defaultValue={info.firstName}
        validationMsgs={['Måste vara längre än 1 bokstav']}
      />

      <InputWrapper
        title='Efternamn'
        inputFunc={(v) => fieldChange('lastName', v)}
        validator={(value) => value.length > 1}
        defaultValue={info.lastName}
        validationMsgs={['Måste vara längre än 1 bokstav']}
      />
      <InputWrapper
        title='Mail'
        inputFunc={(v) => fieldChange('email', v)}
        validator={(value) => emailRegex.test(value)}
        defaultValue={info.email}
        validationMsgs={['Måste vara en korrekt email-adress']}
      />
      <InputWrapper
        title='Lösenord'
        type='password'
        defaultValue={info.password}
        inputFunc={(v) => fieldChange('password', v)}
        validator={(value) => value.length > 6}
        validationMsgs={['Minst 6 bokstäver/symboler/nummer', 'minst 1 bokstav', 'minst 1 symbol']}
      />
      <InputWrapper
        title='Bekräfta Lösenord'
        type='password'
        defaultValue={info.password}
        inputFunc={(v) => fieldChange('confirmPassword', v)}
        validator={(value) => value === info.password}
        validationMsgs={['Måste vara samma som föregående lösenord']}
      />
      <InputWrapper
        title='Födelsedatum'
        type='date'
        inputFunc={(v) => fieldChange('dateOfBirth', v)}
        defaultValue={info.dateOfBirth}
        validator={(value) => value.length > 1}
      />
      <InputWrapper
        title='Land'
        inputFunc={(v) => fieldChange('countryOfResidence', v)}
        validator={(value) => value.length > 1}
        defaultValue={info.countryOfResidence}
        validationMsgs={['Ett lands svenska namn']}
      />
      <InputWrapper
        title='Postnummer'
        inputFunc={(v) => fieldChange('zipCode', v)}
        defaultValue={info.zipCode}
        validator={(value) => value.length > 1}
      />
      <InputWrapper
        title='Telefonnummer'
        inputFunc={(v) => fieldChange('contactNumber', v)}
        defaultValue={info.contactNumber}
        validator={(value) => (value.length === 12 && value.charAt(0) === '+') || (value.length === 10 && /[0-9]*/.test(value))}
        validationMsgs={['I formatet 0701216151']}
      />
      <div className={styles.buttonPos}>
        <Button onClickFunction={() => submit()} title={register ? 'Registrera' : 'Spara'} />
      </div>
    </div>
  )
}
