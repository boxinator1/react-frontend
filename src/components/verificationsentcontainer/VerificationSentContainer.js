import React from 'react'
import Logo from '../logo/Logo'
import styles from './VerificationSentContainer.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRedo } from '@fortawesome/free-solid-svg-icons'
export default function VerificationSentContainer ({ email, reset }) {
  return (
    <div className={styles.innerContainer}>
      <Logo />
      <p className={styles.bigText}>Vi har skickat ett verifikations mail till {email}!<br /> Var vänligen klicka på länken i mailet så skickas din order.</p>
      <div className={styles.iconHolder}>
        <FontAwesomeIcon icon={faRedo} size='4x' className={styles.fa} onClick={reset} />
        <label>Skicka igen</label>
      </div>
      <p className={styles.smallerText}>Psst, om du vill se alla din ordrar och deras status så kan du skapa en användare uppe till vänster!</p>
    </div>
  )
}
